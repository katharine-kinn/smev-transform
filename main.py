import re

from xml.etree import ElementTree as ET
from xml.dom import minidom


def smev_transform(root):
    impl = minidom.getDOMImplementation()
    xmldoc = None

    iprefix = 1
    # namespaces = {}
    parents = {}
    parent_namespaces = {}
    namespaces = {}

    for e in root.iter():
        m = re.match(r'^{(.+)}(.+)$', e.tag)
        ns = m.group(1)
        tag = m.group(2)

        declared = False
        parent_namespace = parent_namespaces.get(e.tag)
        if parent_namespace is not None and parent_namespace != ns:
            iprefix += 1
            declared = True

        i = namespaces.get(ns)
        if not i or declared:
            namespaces[ns] = iprefix

        prefix = 'ns{}'.format(namespaces[ns])

        qname = '{}:{}'.format(prefix, tag)
        if not xmldoc:
            xmldoc = impl.createDocument(ns, qname, None)

        parent = xmldoc.documentElement
        if e.tag in parents:
            parent = parents[e.tag]

        children = list(e)

        dom_element = xmldoc.createElement(qname)
        if declared:
            xmlns_name = 'xmlns:{}'.format(prefix)
            dom_element.setAttribute(xmlns_name, ns)
        has_text = e.text and re.match(r'[^\s\n]+', e.text)
        if has_text:
            text = xmldoc.createTextNode(e.text)
            dom_element.appendChild(text)

        if not children and not has_text:
            dom_element.appendChild(xmldoc.createTextNode(''))

        #  TODO: add sorted attributes

        parent.appendChild(dom_element)

        for c in children:
            parents[c.tag] = dom_element
            parent_namespaces[c.tag] = ns

    return xmldoc


if __name__ == '__main__':
    tree = ET.parse('test.xml')
    root = tree.getroot()
    newxml = smev_transform(root)
    print(newxml.toprettyxml())
